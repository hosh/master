I have got log in a few different scenarios
For each scenario a NN is trained and its model saved
At the test phase all models will be load and the motion model is the average output of all the trained NN

# How Scenarios are designed 
(In first four scenarios casters are placed in left(),right())


1- Imperfect - First Scenario (left wheel is diverged 5degree toward right wheel and also pose is different)

	Orientation 					Position
	
	left (-90,-85,+90)				left(-0.47,-0.22) 
	right (0,-90, 180)				right(0.5,-0.3)

2- Imperfect - Second Scenario (left wheel is diverged 5degree toward right wheel, but position same as right)

	Orientation 					Position
	
	left (-90,-85,+90)				left(0.5,-0.3) 
	right (0,-90, 180)				right(0.5,-0.3)


3- Imperfect - Third Scenario (right wheel is diverged 5degree toward left wheel, but position same as right)

	Orientation 					Position
	
	left (0,-90,+180)				left(0.5,-0.3) 
	right (90,-85, -90)				right(0.5,-0.3)

4- Imperfect - Fourth Scenario (right wheel is diverged 5degree toward left wheel, position also is different)

	Orientation 					Position
	
	left (0,-90,+180)				left(0.5,-0.3) 
	right (90,-85, -90)				right(0.47,-0.20)

5- Imperfect - fifth Scenario (actuators positioned perfectly)

	Casters Pose 					
	
	forgot to save :)


6- Imperfect - Sixth Scenario (actuators positioned perfectly)

	Casters Pose 					
	
	left (-1.22,0.25)				
	right (1.3,0.3)	

7- FirstPerfect  (symmetric)

	Caster Pose 				Joint Position
	
	left (-1.2,.3)				left(-0.5,-0.3) 
	right (1.2,.3)				right(0.5,-0.3)

8- imperfect1  (joints like FirsPerfect)

	Caster Pose 					
	
	left (-1.2,.3)				 
	right (1.1,.4)

9- imperfect2  (joints like FirsPerfect)

	Caster Pose 					
	
	left (-1.1,.4)				 
	right (1.2,.3)	

10 - imperfect3 (positions perfect but joint left 5degree diverged toward joint right)

11 - imperfect3 (positions perfect but joint right 5degree diverged toward joint left)						
