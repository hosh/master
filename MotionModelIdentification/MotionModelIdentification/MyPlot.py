#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 13:24:57 2020
        This script is used to plot the data
@author: hossein
"""
import matplotlib.pyplot as plt
import numpy as np

def pltRawData( x , y , theta):
        """
        plots the x trajectory, y trajectory, robot heading angle, and robot trajectory in the X_Y plane
        """
        fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        steps = np.arange(len(x))
        ax = fig.subplots(2, 2)
        ax[0, 0].plot(steps, x,label='x')
        ax[1, 0].plot(steps, y,label='y')
        ax[0, 1].plot(steps,theta,label='theta')
        ax[1, 1].plot(x,y,label='x-y')
        ax[0, 0].legend()
        ax[1, 0].legend()
        ax[0, 1].legend()
        ax[1, 1].legend()
        plt.show()

def pltPredictedData(predicted , real ,save_add):
        """
        predicted: out put of Neural Network
        real: what Neural network must predicts(Ground Truth Data)
        plots the predicted vs Ground truth for the robot trajectory 
        """
        fig = plt.figure(num=0, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        steps = np.arange(len(real))
        ax = fig.subplots(3,1)
        
        ax[0].plot(steps,real[:,0],label='X-GT')
        ax[0].plot(steps,predicted[:,0],label='X-Pred')
        ax[0].legend();

        ax[1].plot(steps,real[:,1],label='Y-GT')
        ax[1].plot(steps,predicted[:,1],label='Y-Pred')
        ax[1].legend();


        ax[2].plot(steps,real[:,2],label='Theta-GT')
        ax[2].plot(steps,predicted[:,2],label='Theta-Pred')
        ax[2].legend();
        if save_add:
                plt.savefig(save_add)
        plt.show()
