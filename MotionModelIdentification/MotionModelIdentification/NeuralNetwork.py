#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 13:45:21 2020
        This scripts implements two different Neural network by using Keras
        1- Multilayer Perceptron
        2- Recursive Neural Networl
        to predicts the motion model of a differential drive mobile robot
@author: hossein
"""
from tensorflow import keras
class NeuralNetwork:
        def __init__(self,*,NN_TYPE):
                """
                NN_TYPE could be "FFNN" (Feed Forward NN) or "RNN" (Recursive NN)
                """
                self.Models = []
                self.NN_TYPE=NN_TYPE
                print("Creating a " + self.NN_TYPE +" ...")
                
        def creatNNModel(self,input_dim):
                if self.NN_TYPE=="FFNN":
                        model = keras.models.Sequential()
                        model.add(keras.layers.Dense(units=16, input_shape=input_dim, kernel_initializer='random_uniform', bias_initializer='zeros',activation="relu")) #Fully-connected RNN where the output is to be fed back to input
                        model.add(keras.layers.Dense(12, kernel_initializer='random_uniform', bias_initializer='zeros', activation="relu"))
                        model.add(keras.layers.Dense(3))
                        model.compile(loss='mean_squared_error', optimizer='adam' ,metrics=['accuracy'])
                        model.summary()
                elif self.NN_TYPE=="RNN":
                        model = keras.models.Sequential()
                        model.add(keras.layers.SimpleRNN(units=16, input_shape=input_dim, activation="relu")) #Fully-connected RNN where the output is to be fed back to input
                        model.add(keras.layers.Dense(12, activation="relu")) 
                        model.add(keras.layers.Dense(3))
                        model.compile(loss='mean_squared_error', optimizer='adam' ,metrics=['accuracy'])
                        model.summary()
                self.Models.append(model)
        
        def loadNNModel(self,address):
                """
                most of the time we have to more than one model, so we must keep track on them
                """
                self.Models.append(keras.models.load_model(address))
                
        def trainNN(self,X_train,Y_train,address):
                """
                this function must be called when a model is created not when models are loaded
                so the index of created model is 0
                """
                self.Models[0].fit(X_train,Y_train, epochs=10, batch_size=16)
                if address:
                        self.Models[0].save(address)
        
        def testNN(self,X_test):
                allPredicts = []
                for i in range(len(self.Models)):
                        allPredicts.append(self.Models[i].predict(X_test))
                return allPredicts
                                
