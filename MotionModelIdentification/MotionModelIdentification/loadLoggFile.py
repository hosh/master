#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 14:50:07 2020
        
@author: hossein
"""
import numpy as np

def loadData (address):
        logged = np.loadtxt(address, dtype='str', delimiter=',')
        data = logged.astype(np.float)
        return data

#input is logged data. Uses first five row in each line to excluse robot position and desired velocity. 
#Then splits them to input and output data for NN

def splitLogData(raw_data):
        vL, vR=[],[]
        PoseX, PoseY, PoseTheta  = [], [], []
        x, y=[],[]
        rng = len(raw_data)
        
        for i in range(rng):
                PoseX.append(raw_data[i][0])
                PoseY.append(raw_data[i][1])
                PoseTheta.append(raw_data[i][2])
                vL.append(raw_data[i][3])
                vR.append(raw_data[i][4])
                x.append([PoseX[i],PoseY[i],PoseTheta[i],vL[i],vR[i]])
        for i in range(rng-1):
                y.append([PoseX[i+1],PoseY[i+1],PoseTheta[i+1]])
        #pltRawData(PoseX,PoseY,PoseTheta)
        return np.asarray(x),np.asarray(y) 