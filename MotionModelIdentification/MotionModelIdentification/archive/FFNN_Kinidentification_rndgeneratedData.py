#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 00:14:09 2020

@author: hossein
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from keras.models import Sequential
from keras.layers import Dense, SimpleRNN
import random

output_layer_size =3
step = 1
total_timesteps = 2000    
train_timesteps = 1600 
timeStep = 0.25
thetaMax = np.pi*2
periodOfData = 5 # [x,y,theta,vL,vR]



def pltRawData( x , y , theta):
        fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        T = np.arange(len(x))
        ax = fig.subplots(2, 2)
        
        ax[0, 0].plot(T, x,label='x')
        ax[1, 0].plot(T, y,label='y')
        ax[0, 1].plot(T,theta,label='theta')
        ax[1, 1].plot(x,y,label='x-y')
        ax[0, 0].legend()
        ax[1, 0].legend()
        ax[0, 1].legend()
        ax[1, 1].legend()
        plt.show()

def pltPredictedData(predicted , real , lable):
        figure(num=0, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        time = np.arange(total_timesteps-train_timesteps)
        plt.figure(0)
        plt.plot(time,real[1600:1600+len(time)])
        predPose = []
        if lable=="x" :
                predPose=predicted[:,0]
        elif lable=="y":
                predPose=predicted[:,1]
        elif lable=="theta":
                predPose=predicted[:,2]
        plt.plot(time[0:len(predPose)],predPose)
        plt.legend( ['Ground Truth', 'Predicted']);
        plt.show()

#generate random Velocities (m/s) for left and right actuator and calculate Linear and Angular Velocity based
random.seed(0)
vL=[]
vR=[]
LinearV=[]
AngularV=[]
for i in range(total_timesteps):
        vL.append(random.uniform(-1,1))
        vR.append(random.uniform(-1,1))
        LinearV.append((vL[i]+vR[i])/2)
        AngularV.append((vL[i]-vR[i]))


#concate all the data as [x, y, theta, vL, vR, x, y, theta, vL, vR, ...]
PoseX = [0]
PoseY = [0]
PoseTheta = [0]
x=[]
y = []
for i in range(total_timesteps):
        
        xTemp = [PoseX[i],PoseY[i],PoseTheta[i],vL[i],vR[i]]
        x.append(xTemp)
        PoseX.append(PoseX[i]+LinearV[i]*timeStep*np.cos(PoseTheta[i]))
        PoseY.append(PoseY[i]+LinearV[i]*timeStep*np.sin(PoseTheta[i]))
        PoseTheta.append(PoseTheta[i]+AngularV[i]*timeStep)
        if PoseTheta[i+1]>thetaMax:
                PoseTheta[i+1]-=thetaMax
        elif PoseTheta[i+1]<-thetaMax:
                PoseTheta[i+1]+=thetaMax
        
        yTemp = [PoseX[i+1],PoseY[i+1],PoseTheta[i+1]]
        y.append(yTemp)
        




x = np.asarray(x)
y= np.asarray(y)



pltRawData(PoseX,PoseY,PoseTheta)

xtrain,xtest = x[0:train_timesteps,:], x[train_timesteps:total_timesteps,:]
ytrain,ytest = y[0:train_timesteps,:], y[train_timesteps:total_timesteps,:]


trainX,trainY = xtrain, ytrain
testX,testY = xtest , ytest


in_dim = trainX.shape[1:3]

model = Sequential()
#units: Positive integer, dimensionality of the output space
model.add(Dense(units=8, input_shape=in_dim, activation="relu")) #Fully-connected RNN where the output is to be fed back to input
model.add(Dense(8, activation="relu")) 
model.add(Dense(output_layer_size))
model.compile(loss='mean_squared_error', optimizer='adam')
model.summary()

model.fit(trainX,trainY, epochs=100, batch_size=16, verbose=2)
trainPredict = model.predict(trainX)
testPredict= model.predict(testX)
predicted=np.concatenate((trainPredict,testPredict),axis=0)

trainScore = model.evaluate(trainX, trainY, verbose=0)
print(trainScore)

pltPredictedData(testPredict , PoseX , "x")
pltPredictedData(testPredict , PoseY , "y")
pltPredictedData(testPredict , PoseTheta , "theta")
