#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 13:25:09 2020
This scripts loads all the saved model (.h5) and tries to predict a desired scenario
by fedding to all loaded models, and calculating the average of all models
Note: Vrep gives the robots heading angle theta relative to Y axis 
@author: hossein
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import tensorflow as tf

trainedModelsName = ['imperfect1.h5','imperfect2.h5','imperfect3.h5','imperfect4.h5'
                     ,'imperfect5.h5','imperfect6.h5','firstPerfect.h5'
                     ,'imperfectFirstScenario.h5','imperfectSecondScenario.h5'
                     ,'imperfectThirdScenario.h5','imperfectFourthScenario.h5'
                     ,'imperfectFifthScenario.h5','imperfectSixthScenario.h5'
                     ,'imperfectSeventhScenario.h5','perfectScenario.h5'
                     ,'perfectSecondScenario.h5']       
baseAddress = '/home/hossein/Desktop/Master/Codes/master/MotionModelIdentification/MotionModelIdentification/LogDataForTrainAndTest/'
TestFileAddress  = baseAddress+'log/randomTest1.txt'



#input is address of the logg file and output is logged data
def loadData (address):
        logged = np.loadtxt(address, dtype='str', delimiter=',')
        data = logged.astype(np.float)
        return data

#input is logged data. Uses first five row in each line to excluse robot position and desired velocity. 
#Then splits them to input and output data for NN

def splitLogData(data):
        vL, vR=[],[]
        PoseX, PoseY, PoseTheta  = [], [], []
        x, y=[],[]
        rng = len(data)
        
        for i in range(rng):
                PoseX.append(data[i][0])
                PoseY.append(data[i][1])
                PoseTheta.append(data[i][2])
                vL.append(data[i][3])
                vR.append(data[i][4])
                x.append([PoseX[i],PoseY[i],PoseTheta[i],vL[i],vR[i]])
        for i in range(rng-1):
                y.append([PoseX[i+1],PoseY[i+1],PoseTheta[i+1]])
        pltRawData(PoseX,PoseY,PoseTheta)
        return np.asarray(x),np.asarray(y) 


def normalizer(data):
        maxData = np.amax(data)
        minData = np.amin(data)
        return (data-minData)/(maxData-minData)

def pltRawData( x , y , theta):
        fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        T = np.arange(len(x))
        ax = fig.subplots(2, 2)
        ax[0, 0].plot(T, x,label='x')
        ax[1, 0].plot(T, y,label='y')
        ax[0, 1].plot(T,theta,label='theta')
        ax[1, 1].plot(x,y,label='x-y')
        ax[0, 0].legend()
        ax[1, 0].legend()
        ax[0, 1].legend()
        ax[1, 1].legend()
        plt.show()

def pltPredictedData(predicted , real ,save_add):
        fig = plt.figure(num=0, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        time = np.arange(len(real))
        ax = fig.subplots(3,1)
        
        ax[0].plot(time,real[:,0],label='X-GT')
        ax[0].plot(time,predicted[:,0],label='X-Pred')
        ax[0].legend();

        ax[1].plot(time,real[:,1],label='Y-GT')
        ax[1].plot(time,predicted[:,1],label='Y-Pred')
        ax[1].legend();


        ax[2].plot(time,real[:,2],label='Theta-GT')
        ax[2].plot(time,predicted[:,2],label='Theta-Pred')
        ax[2].legend();
        plt.savefig(save_add.replace('.h5','.png'))
        plt.show()

def load_trained_model(trained_model_add):
        new_model = tf.keras.models.load_model(trained_model_add)
        return new_model

testLog  = loadData(TestFileAddress)

testInput , testOutput = splitLogData(testLog)

testX,testY = testInput , testOutput


testX = testX[:len(testX)-1,:]
allPredicts = []
numberOfModels =2#len(trainedModelsName)
for i in range(numberOfModels):
        model=load_trained_model(baseAddress+trainedModelsName[i])
        testPredict= model.predict(testX)
        allPredicts.append(testPredict)
        pltPredictedData(testPredict , testY,baseAddress+trainedModelsName[i])
  
avgPredict = []
for j in range(len(testPredict)):
        x=0
        y=0
        th=0
        for i in range(numberOfModels):
                x+=allPredicts[i][j][0]
                y+=allPredicts[i][j][1]
                th+=allPredicts[i][j][2]
        avgPredict.append([x/numberOfModels,y/numberOfModels,th/numberOfModels])
        
pltPredictedData(np.asarray(avgPredict) , testY,baseAddress+"avg")
