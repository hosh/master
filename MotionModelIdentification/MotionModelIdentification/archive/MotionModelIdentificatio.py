#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 15:06:24 2020

@author: hossein
"""


import numpy as np
import plot
from matplotlib.pyplot import figure
import tensorflow as tf

trainedModelsName = ['imperfect1.h5','imperfect2.h5','imperfect3.h5','imperfect4.h5'
                     ,'imperfect5.h5','imperfect6.h5','firstPerfect.h5'
                     ,'imperfectFirstScenario.h5','imperfectSecondScenario.h5'
                     ,'imperfectThirdScenario.h5','imperfectFourthScenario.h5'
                     ,'imperfectFifthScenario.h5','imperfectSixthScenario.h5'
                     ,'imperfectSeventhScenario.h5','perfectScenario.h5'
                     ,'perfectSecondScenario.h5']       
baseAddress = '/home/hossein/Desktop/Master/Codes/master/MotionModelIdentification/MotionModelIdentification/LogDataForTrainAndTest/'
TestFileAddress  = baseAddress+'log/randomTest1.txt'



#input is address of the logg file and output is logged data
def loadData (address):
        logged = np.loadtxt(address, dtype='str', delimiter=',')
        data = logged.astype(np.float)
        return data

#input is logged data. Uses first five row in each line to excluse robot position and desired velocity. 
#Then splits them to input and output data for NN

def splitLogData(data):
        vL, vR=[],[]
        PoseX, PoseY, PoseTheta  = [], [], []
        x, y=[],[]
        rng = len(data)
        
        for i in range(rng):
                PoseX.append(data[i][0])
                PoseY.append(data[i][1])
                PoseTheta.append(data[i][2])
                vL.append(data[i][3])
                vR.append(data[i][4])
                x.append([PoseX[i],PoseY[i],PoseTheta[i],vL[i],vR[i]])
        for i in range(rng-1):
                y.append([PoseX[i+1],PoseY[i+1],PoseTheta[i+1]])
        plot.pltRawData(PoseX,PoseY,PoseTheta)
        return np.asarray(x),np.asarray(y) 


def normalizer(data):
        maxData = np.amax(data)
        minData = np.amin(data)
        return (data-minData)/(maxData-minData)



def load_trained_model(trained_model_add):
        new_model = tf.keras.models.load_model(trained_model_add)
        return new_model

testLog  = loadData(TestFileAddress)

testInput , testOutput = splitLogData(testLog)

testX,testY = testInput , testOutput


testX = testX[:len(testX)-1,:]
allPredicts = []
numberOfModels =2#len(trainedModelsName)
for i in range(numberOfModels):
        model=load_trained_model(baseAddress+trainedModelsName[i])
        testPredict= model.predict(testX)
        allPredicts.append(testPredict)
        plot.pltPredictedData(testPredict , testY,baseAddress+trainedModelsName[i])
  
avgPredict = []
for j in range(len(testPredict)):
        x=0
        y=0
        th=0
        for i in range(numberOfModels):
                x+=allPredicts[i][j][0]
                y+=allPredicts[i][j][1]
                th+=allPredicts[i][j][2]
        avgPredict.append([x/numberOfModels,y/numberOfModels,th/numberOfModels])
        
plot.pltPredictedData(np.asarray(avgPredict) , testY,baseAddress+"avg")
