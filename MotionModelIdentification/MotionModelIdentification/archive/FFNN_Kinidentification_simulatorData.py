#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Saturday Apr  11 15:11:50 2020
        This script uses a Feed Forward Neural Network for motion model (kinematic Identification)
        for a robot. 
        The input/output of this FFNN is as follow:
                input  = [X Y theta commandedVelocityLeftWheel commandedVelocityLeftWheel]
                output = [nextX nextY nextTheta]
        A simple simulation in V-rep is done to simulate a platform (differential drive robot) as what we will have in Vadarstad Factory
        Two different Scenario are designed in V-rep. One of them will be used for training and another will be used for the test of FFNN
@author: Hossein
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from keras.models import Sequential
from keras.layers import Dense

TrainFileAddress = '/home/hossein/Desktop/Master/Codes/master/MotinModelIdentification/LogDataForTrainAndTest/imperfectTrainLog.txt'
TestFileAddress  = '/home/hossein/Desktop/Master/Codes/master/MotinModelIdentification/LogDataForTrainAndTest/imperfectTestLog5.txt'


#input is address of the logg file and output is logged data
def loadData (address):
        logged = np.loadtxt(address, dtype='str', delimiter=',')
        data = logged.astype(np.float)
        return data

#input is logged data. Uses first five row in each line to excluse robot position and desired velocity. 
#Then splits them to input and output data for NN
def splitLogData(data):
        vL, vR=[],[]
        PoseX, PoseY, PoseTheta  = [], [], []
        x, y=[],[]
        rng = len(data)
        
        for i in range(rng):
                PoseX.append(data[i][0])
                PoseY.append(data[i][1])
                PoseTheta.append(data[i][2])
                vL.append(data[i][3])
                vR.append(data[i][4])
                x.append([PoseX[i],PoseY[i],PoseTheta[i],vL[i],vR[i]])
        
        for i in range(rng-1):
                y.append([PoseX[i+1],PoseY[i+1],PoseTheta[i+1]])
        pltRawData(PoseX,PoseY,PoseTheta)
        return np.asarray(x),np.asarray(y)


def normalizer(data):
        maxData = np.amax(data)
        minData = np.amin(data)
        return (data-minData)/(maxData-minData)

def pltRawData( x , y , theta):
        fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        T = np.arange(len(x))
        ax = fig.subplots(2, 2)
        ax[0, 0].plot(T, x,label='x')
        ax[1, 0].plot(T, y,label='y')
        ax[0, 1].plot(T,theta,label='theta')
        ax[1, 1].plot(x,y,label='x-y')
        ax[0, 0].legend()
        ax[1, 0].legend()
        ax[0, 1].legend()
        ax[1, 1].legend()
        plt.show()

def pltPredictedData(predicted , real , lable):
        figure(num=0, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        time = np.arange(len(real))
        plt.figure(0)
        if lable=="x" :
                plt.plot(time,real[:,0])
                plt.plot(time,predicted[:,0])
                plt.legend( ['X-Ground Truth', 'X-Predicted']);

        elif lable=="y":
                plt.plot(time,real[:,1])
                plt.plot(time,predicted[:,1])
                plt.legend( ['Y-Ground Truth', 'Y-Predicted']);

        elif lable=="theta":
                plt.plot(time,real[:,2])
                plt.plot(time,predicted[:,2])
                plt.legend( ['Theta-Ground Truth', 'Theta-Predicted']);

        plt.show()



trainLog = loadData(TrainFileAddress)
testLog  = loadData(TestFileAddress)
output_layer_size =3



tranInput,trainOutput = splitLogData(trainLog)
testInput , testOutput = splitLogData(testLog)

trainX,trainY = tranInput, trainOutput
testX,testY = testInput , testOutput

in_dim = trainX.shape[1:3]


model = Sequential()
model.add(Dense(units=16, input_shape=in_dim, kernel_initializer='random_uniform', bias_initializer='zeros',activation="relu")) #Fully-connected RNN where the output is to be fed back to input
model.add(Dense(12, kernel_initializer='random_uniform', bias_initializer='zeros', activation="relu"))
model.add(Dense(output_layer_size))
model.compile(loss='mean_squared_error', optimizer='adam' ,metrics=['accuracy'])
model.summary()

trainX = trainX[:len(trainX)-1,:]
testX = testX[:len(testX)-1,:]
history = model.fit(trainX,trainY, epochs=10, batch_size=16)
trainPredict = model.predict(trainX)
testPredict= model.predict(testX)

pltPredictedData(trainPredict , trainY , "x")
pltPredictedData(trainPredict , trainY , "y")
pltPredictedData(trainPredict , trainY , "theta")

pltPredictedData(testPredict , testY , "x")
pltPredictedData(testPredict , testY , "y")
pltPredictedData(testPredict , testY , "theta")

