#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 00:10:07 2020

@author: hossein
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 20:11:50 2020

@author: hossein
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from keras.models import Sequential
from keras.layers import Dense, SimpleRNN


TrainFileAddress = '/home/hossein/Desktop/Master/Codes/master/MotinModelIdentification/LogDataForTrainAndTest/perfectTrainLog.txt'
TestFileAddress  = '/home/hossein/Desktop/Master/Codes/master/MotinModelIdentification/LogDataForTrainAndTest/perfectTestLog.txt'

output_layer_size =3
step = 5
periodOfData = 5 # [x,y,theta,vL,vR]

# convert into dataset matrix
def convertToMatrix(xdata,ydata, step):
 X, Y =[], []
 for i in range(len(xdata)-step):
  d=i+step  
  X.append(xdata[i:d])
  Y.append(ydata[d-1])
 return np.array(X), np.array(Y)


#input is address of the logg file and output is logged data
def loadData (address):
        logged = np.loadtxt(address, dtype='str', delimiter=',')
        data = logged.astype(np.float)
        return data

#input is logged data. Uses first five row in each line to excluse robot position and desired velocity. 
#Then splits them to input and output data for NN
def splitLogData(data):
        vL, vR=[],[]
        PoseX, PoseY, PoseTheta  = [], [], []
        x, y=[],[]
        rng = len(data)
        
        for i in range(rng):
                PoseX.append(data[i][0])
                PoseY.append(data[i][1])
                PoseTheta.append(data[i][2])
                vL.append(data[i][3])
                vR.append(data[i][4])
                x.append([PoseX[i],PoseY[i],PoseTheta[i],vL[i],vR[i]])
        
        for i in range(rng-1):
                y.append([PoseX[i+1],PoseY[i+1],PoseTheta[i+1]])
        pltRawData(PoseX,PoseY,PoseTheta)
        return np.asarray(x),np.asarray(y)


def normalizer(data):
        maxData = np.amax(data)
        minData = np.amin(data)
        return (data-minData)/(maxData-minData)

def pltRawData( x , y , theta):
        fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        T = np.arange(len(x))
        ax = fig.subplots(2, 2)
        ax[0, 0].plot(T, x,label='x')
        ax[1, 0].plot(T, y,label='y')
        ax[0, 1].plot(T,theta,label='theta')
        ax[1, 1].plot(x,y,label='x-y')
        ax[0, 0].legend()
        ax[1, 0].legend()
        ax[0, 1].legend()
        ax[1, 1].legend()
        plt.show()

def pltPredictedData(predicted , real , lable):
        figure(num=0, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
        time = np.arange(len(real))
        plt.figure(0)
        if lable=="x" :
                plt.plot(time,real[:,0])
                plt.plot(time[0:len(predicted)],predicted[:,0])
                plt.legend( ['X-Ground Truth', 'X-Predicted']);

        elif lable=="y":
                plt.plot(time,real[:,1])
                plt.plot(time[0:len(predicted)],predicted[:,1])
                plt.legend( ['Y-Ground Truth', 'Y-Predicted']);

        elif lable=="theta":
                plt.plot(time,real[:,2])
                plt.plot(time[0:len(predicted)],predicted[:,2])
                plt.legend( ['Theta-Ground Truth', 'Theta-Predicted']);

        plt.show()



trainLog = loadData(TrainFileAddress)
testLog  = loadData(TestFileAddress)
output_layer_size =3



tranInput,trainOutput = splitLogData(trainLog)
testInput , testOutput = splitLogData(testLog)

trainX,trainY = convertToMatrix(tranInput, trainOutput,step)
testX,testY = convertToMatrix(testInput , testOutput,step)


in_dim = trainX.shape[1:3]

model = Sequential()
#units: Positive integer, dimensionality of the output space
model.add(SimpleRNN(units=16, input_shape=in_dim, activation="relu")) #Fully-connected RNN where the output is to be fed back to input
model.add(Dense(12, activation="relu")) 
model.add(Dense(output_layer_size))
model.compile(loss='mean_squared_error', optimizer='adam' ,metrics=['accuracy'])
model.summary()

testX = testX[:len(testX)-1,:]
history = model.fit(trainX,trainY, epochs=10, batch_size=16)
trainPredict = model.predict(trainX)
testPredict= model.predict(testX)

pltPredictedData(trainPredict , trainY , "x")
pltPredictedData(trainPredict , trainY , "y")
pltPredictedData(trainPredict , trainY , "theta")

pltPredictedData(testPredict , testY , "x")
pltPredictedData(testPredict , testY , "y")
pltPredictedData(testPredict , testY , "theta")

