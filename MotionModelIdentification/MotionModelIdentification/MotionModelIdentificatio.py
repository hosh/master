#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 15:06:24 2020

@author: hossein
"""


import numpy as np
import MyPlot as plot
import NeuralNetwork 
import loadLoggFile as lg
BASEAddress = '/home/hossein/Desktop/Master/Codes/master/MotionModelIdentification/MotionModelIdentification/LogDataForTrainAndTest/'
TestFileAddress  = BASEAddress+'log/randomTest1.txt'
TrainFileAddress = BASEAddress+'log/imperfect6Train.txt'

trainedModelsName = ['imperfect1.h5','imperfect2.h5','imperfect3.h5','imperfect4.h5'
                     ,'imperfect5.h5','imperfect6.h5','firstPerfect.h5'
                     ,'imperfectFirstScenario.h5','imperfectSecondScenario.h5'
                     ,'imperfectThirdScenario.h5','imperfectFourthScenario.h5'
                     ,'imperfectFifthScenario.h5','imperfectSixthScenario.h5'
                     ,'imperfectSeventhScenario.h5','perfectScenario.h5'
                     ,'perfectSecondScenario.h5']
NUMOFMODELS = len(trainedModelsName)

def trainNewModel():
        """
        load and prepare data for the training
        """
        raw_data_test = lg.loadData(TestFileAddress) #load data for test
        raw_data_train = lg.loadData(TrainFileAddress) #load data for train
        X_test , Y_test = lg.splitLogData(raw_data_test)
        X_train , Y_train = lg.splitLogData(raw_data_train)
        
        plot.pltRawData(X_train[:,0],X_train[:,1],X_train[:,2])
        
        in_dim = X_train.shape[1:3]
        nn = NeuralNetwork.NeuralNetwork(NN_TYPE="FFNN")
        nn.creatNNModel(in_dim)
        X_train = X_train[:len(X_train)-1,:]
        X_test = X_test[:len(X_test)-1,:]
        nn.trainNN(X_train,Y_train,"")
        
        
        train_predict = nn.testNN(X_train)
        test_predict = nn.testNN(X_test)
        
        plot.pltPredictedData(train_predict[0],Y_train,"")
        plot.pltPredictedData(test_predict[0],Y_test,"")
        
def loadTrainedModelsAndPredict():
        """
        load and prepare data for the test phase
        """
        raw_data = lg.loadData(TestFileAddress) #load data for test
        X_ , Y_ = lg.splitLogData(raw_data)
        plot.pltRawData(X_[:,0],X_[:,1],X_[:,2])
        X_ = X_[:len(X_)-1,:]
        
        """
        load pre-trained NN and get their prediction
        """
        nn = NeuralNetwork.NeuralNetwork(NN_TYPE="FFNN")
        for i in range(NUMOFMODELS):#load all models
                nn.loadNNModel(BASEAddress+trainedModelsName[i])
        
        allPredicts=nn.testNN(X_) #it would train all the loaded models
        for i in range (NUMOFMODELS):
                plot.pltPredictedData(allPredicts[i],Y_,"")
        
        """
        calculate average of all the predictions
        """
        avgPredict = []
        for j in range(len(Y_)):
                x=0
                y=0
                th=0
                for i in range(NUMOFMODELS):
                        x+=allPredicts[i][j][0]
                        y+=allPredicts[i][j][1]
                        th+=allPredicts[i][j][2]
                avgPredict.append([x/NUMOFMODELS,y/NUMOFMODELS,th/NUMOFMODELS]) 
        plot.pltPredictedData(np.asarray(avgPredict),Y_,"")



if __name__ == "__main__":
        trainNewModel()
        #loadTrainedModelsAndPredict()

